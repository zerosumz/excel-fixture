package com.aperturesoft;

import java.io.IOException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

import static org.springframework.util.Assert.*;

public class ExcelFixtureConfigurer implements BeanDefinitionRegistryPostProcessor, InitializingBean,
		ApplicationContextAware, BeanNameAware {
	private ApplicationContext applicationContext;
	private String beanName;
	private Resource[] excelFiles;
	private DataSource dataSource;
	private List<ExcelToSqlRunner> excelToSqlRunners;

	/**
	 * after bean factory init
	 * 
	 * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
	 */
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}

	/**
	 * after properties set
	 * 
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		notNull(excelFiles, "excelFiles empty!");
		notNull(dataSource, "datasource ref please!");
		try {
			this.excelToSqlRunners = ExcelFixtureMain.createExcelFixture(dataSource, excelFiles);
		} catch (IOException e) {
			e.printStackTrace();
			throw new BeanCreationException(e.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new BeanCreationException(e.getMessage());
		}
	}

	/**
	 * after <b>this</b> dependency init
	 * 
	 * @see org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor#postProcessBeanDefinitionRegistry(org.springframework.beans.factory.support.BeanDefinitionRegistry)
	 */
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;

	}

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public Resource[] getExcelFiles() {
		return excelFiles;
	}

	public void setExcelFiles(Resource[] excelFiles) {
		this.excelFiles = excelFiles;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public List<ExcelToSqlRunner> getExcelToSqlRunners() {
		return excelToSqlRunners;
	}

	public void setExcelToSqlRunners(List<ExcelToSqlRunner> excelToSqlRunners) {
		this.excelToSqlRunners = excelToSqlRunners;
	}

};
