package com.aperturesoft;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.Resource;

public class ExcelFixtureMain {
	public static List<ExcelToSqlRunner> createExcelFixture(DataSource dataSource, Resource[] excelFiles) throws IOException, ClassNotFoundException {
		List<ExcelToSqlRunner> excelToSqlRunners = new ArrayList<ExcelToSqlRunner>();
		for (Resource resource : excelFiles) {
			Workbook wb = new HSSFWorkbook(resource.getInputStream());
			int j = wb.getNumberOfSheets();
			for (int i = 0; i < j; i++) {
				Sheet sheet = wb.getSheetAt(i);
				String sheetName = sheet.getSheetName();
				Row headderRow = sheet.getRow(0);
				Row headerTypeRow = sheet.getRow(1);
				Iterator<Cell> iterator = headderRow.iterator();
				Iterator<Cell> typeIterator = headerTypeRow.iterator();
				LinkedHashMap<String, Class<?>> headerMap = new LinkedHashMap<String, Class<?>>();
				while (iterator.hasNext()) {
					Cell headerCell = iterator.next();
					Cell typeCell = typeIterator.next();
					String columnName = headerCell.getStringCellValue();
					Class<?> columnType = Class.forName(typeCell.getStringCellValue());
					headerMap.put(columnName, columnType);
				}
				
				ExcelToSqlRunner excelToSqlRunner = new ExcelToSqlRunner(dataSource, sheetName, headerMap);
				Iterator<Row> rowIter = sheet.iterator();
				rowIter.next();
				rowIter.next();
				excelToSqlRunner.saveRows(rowIter);
				excelToSqlRunners.add(excelToSqlRunner);
			}
		}
		return excelToSqlRunners;
	}
}
