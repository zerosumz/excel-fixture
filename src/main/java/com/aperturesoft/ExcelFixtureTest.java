package com.aperturesoft;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

public class ExcelFixtureTest {
	@Autowired
	ExcelFixtureConfigurer configurer;
	
	@Before
	public void insertExcelData(){
		List<ExcelToSqlRunner> excelToSqlRunners = configurer.getExcelToSqlRunners();
		for (ExcelToSqlRunner excelToSqlRunner : excelToSqlRunners) {
			excelToSqlRunner.insertRows();
		}
	}
	
	@After
	public void deleteExcelData(){
		List<ExcelToSqlRunner> excelToSqlRunners = configurer.getExcelToSqlRunners();
		for (ExcelToSqlRunner excelToSqlRunner : excelToSqlRunners) {
			excelToSqlRunner.deleteRows();
		}
	}
}
