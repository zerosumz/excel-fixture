package com.aperturesoft;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.BeanCreationException;

public class ExcelToSqlRunner {
	private DataSource dataSource;
	private String tableName;
	private String sql;
	private LinkedHashMap<String, Class<?>> headerMap;
	private List<List<Object>> excelDatas = new ArrayList<List<Object>>();

	public ExcelToSqlRunner(DataSource dataSource, String tableName, LinkedHashMap<String, Class<?>> headerMap) {
		super();
		this.dataSource = dataSource;
		this.tableName = tableName;
		this.headerMap = headerMap;

		String[] questions = new String[headerMap.size()];
		Arrays.fill(questions, "?");
		sql = "INSERT INTO " + tableName + " ( " + StringUtils.join(headerMap.keySet(), ",") + " ) VALUES ( \n"
				+ StringUtils.join(questions, ",") + " ) ";
	}

	public void saveRows(Iterator<Row> rows) throws BeanCreationException {
		while (rows.hasNext()) {
			Row row = rows.next();
			List<Object> data = new ArrayList<Object>();
			int cellIndex = 0;
			for (Entry<String, Class<?>> entry : headerMap.entrySet()) {
				Class<?> clazz = entry.getValue();
				Cell cell = row.getCell(cellIndex);
				if (clazz.equals(Integer.class))
					data.add((int) cell.getNumericCellValue());
				else if (clazz.equals(Long.class))
					data.add((long) cell.getNumericCellValue());
				else if (clazz.equals(Double.class))
					data.add(cell.getNumericCellValue());
				else if (clazz.equals(Date.class)) {
					data.add(new java.sql.Date(cell.getDateCellValue().getTime()));
				} else if (clazz.equals(Boolean.class))
					data.add(cell.getBooleanCellValue());
				else
					data.add(cell.getStringCellValue());
				cellIndex++;
			}
			excelDatas.add(data);
		}
	}

	public void insertRows() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			for (List<Object> data : excelDatas) {
				preparedStatement = connection.prepareStatement(sql);
				Iterator<Object> iter = data.iterator();
				int paramIndex = 1;
				for (Entry<String, Class<?>> entry : headerMap.entrySet()) {
					Class<?> clazz = entry.getValue();
					Object value = iter.next();
					if (clazz.equals(Integer.class))
						preparedStatement.setInt(paramIndex, (Integer) value);
					else if (clazz.equals(Long.class))
						preparedStatement.setLong(paramIndex, (Long) value);
					else if (clazz.equals(Double.class))
						preparedStatement.setDouble(paramIndex, (Double) value);
					else if (clazz.equals(Date.class)) {
						preparedStatement.setDate(paramIndex, new java.sql.Date(((Date) value).getTime()));
					} else if (clazz.equals(Boolean.class))
						preparedStatement.setBoolean(paramIndex, (Boolean) value);
					else
						preparedStatement.setString(paramIndex, (String) value);

					paramIndex++;
				}

				preparedStatement.execute();
				if (preparedStatement != null)
					preparedStatement.close();
				preparedStatement = null;

			}

		} catch (SQLException e) {
			try {
				e.printStackTrace();
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new BeanCreationException("fail to read excel");

		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public void deleteRows() {
		String sql = "DELETE FROM " + tableName;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}

		}
	}
}
