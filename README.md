Excel fixture for mybatis-spring
================================
what is this
------------
it is very tiny and simple project does insert fixture data for test.
it could handle microsoft excel file.

usage
-----

excel file example

ID				 | AGE 				 | BIRTHDAY
-----------------|-------------------|-----------
java.lang.Integer|java.lang.Integer	 |java.util.Date
1				 |12				 |2001-10-01
2				 |33				 |1981-10-22

rename sheet name to table name to insert 

write some configurations to spring application context;
```
	<bean id="fixture" class="com.aperturesoft.ExcelFixtureConfigurer">
	    <property name="excelFiles">
	        <list>
	            <value>classpath:/a.xls</value>
	        </list>
	    </property>
	    <property name="dataSource" ref="dataSource"/>
	</bean>
```

extend ExcelFixtureTest class should insert excel data before every test methods invoke.

and delete data after method invoked.

```
	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration(value = { "classpath:spring-config-db.xml" })
	public class TestBasicTest extends ExcelFixtureTest{
		@Autowired
		TestMapper testMapper;
	
		@Test
		public void testBasic() {
		    // excel data exist now;
			BirthDay birthDay = testMapper.selectMe(2);
			Assert.assertEquals(33, birthDay.getAge());
			Assert.assertEquals("1981", new SimpleDateFormat("YYYY").format(birthDay.getBirthDay()));
		}
	
	}
```
 
note
----
1. it does not create tables now 

todo
----
1. test runner.

